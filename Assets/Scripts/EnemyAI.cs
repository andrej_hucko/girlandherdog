﻿using UnityEngine;
using Pathfinding;
using System.Collections;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent(typeof (Seeker))]
public class EnemyAI : MonoBehaviour {

    // What to chase
    GameObject dog;
    DogController dogController;

    public Transform target;
    public Transform source;
    public Animator animator;
    // How many times each second we will update our path
    [Tooltip ("How many times per second to update the path")]
    public float updateRate = 2f;

    // Caching
    private Seeker seeker;
    private Rigidbody2D rb;
    bool animationFlip = false;
    // Calculated path
    public Path path;

    // The AI's speed per second
    public float speed = 0.5f;
    public float runSpeed = 10f;

    [HideInInspector]
    public bool pathIsEnded = false;
    public bool isScared = false;
    public bool isCatching = false;

    // Max distance from AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 3;

    // Waypoint we are currently moving towards (index)
    private int currentWaypoint = 0;

    float catchingDistance = 0.2f;
    private void Start()
    {
        dog = GameObject.Find("Dog");
        dogController = dog.GetComponent<DogController>();
        target = dog.transform;
        Debug.Log("Hi");

        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        if (target == null)
        {
            Debug.LogError("No dog found!");
            return;
        }

        // Start a new path to the target position, return the result to the OnPathComplete function
        if (target != null)
            seeker.StartPath(transform.position, target.position, OnPathComplete);

        StartCoroutine(UpdatePath());
    }

    IEnumerator UpdatePath()
    {
        if (target == null)
        {
            // TODO: Insert a player search here
            yield return null;
        }

        seeker.StartPath(transform.position, target.position, OnPathComplete);

        yield return new WaitForSeconds(1f / updateRate);
        if (pathIsEnded)
            yield return null;
        StartCoroutine(UpdatePath());
    }

    public void OnPathComplete(Path p)
    {
        Debug.Log("We got a path. Did it have an error? " + p.error);
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }
    public void MakeScared()
    {
        isScared = true;
    }

    private void Update()
    {
        if (isScared)
        {
            transform.position = Vector2.MoveTowards(transform.position, source.position, runSpeed * Time.deltaTime);
            animator.SetBool("isCatching", false);
        }

        // Flipping animation based on direction
        if (!animationFlip)
        {
            if ((target.position - transform.position).x < 0f)
            {
                transform.eulerAngles = 180f * Vector3.up;
                animationFlip = true;
            }
        }
        else
        {
            if ((target.position - transform.position).x > 0f)
            {
                transform.eulerAngles = 0 * Vector3.up;
                animationFlip = false;
            }
        }
    }

    private void FixedUpdate()
    {
        if (isScared)
        {
            return;
        }

        // If close to Dog we are catching and not moving
        if (Vector2.Distance(transform.position, target.position) < 0.2f)
        {
            animator.SetBool("isCatching", true);
            isCatching = true;
        }
        else
        {
            isCatching = false;
        }

        if (isCatching)
        {
            rb.velocity = Vector2.zero;
            return;
        }

        if (target == null)
        {
            // TODO: Insert a target search here
            return;
        }

        // TODO: Always look at player

        if (path == null) { return; }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            if (pathIsEnded) { return; }

            Debug.Log("End of path reached.");
            pathIsEnded = true;
            return;
        }
        pathIsEnded = false;

        // Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        dir *= speed * Time.fixedDeltaTime;

        // Move the AI
        rb.velocity = new Vector2(Mathf.Lerp(0, dir.x, 0.8f),
                                                Mathf.Lerp(0, dir.y, 0.8f));

        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
        if (dist < nextWaypointDistance)
        { 
            currentWaypoint++;
            return;
        }
    }
}
