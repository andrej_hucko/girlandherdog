﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class InterfaceHandler : MonoBehaviour {

    public GameObject posDog;
    public GameObject posGirl;
    public GameObject endRoundPanel;

    public AudioSource audioSource;
    public AudioClip startSound;
    public AudioClip gongSound;

    public Text scoreValue;
    public Text timerLabel;
    public Text timerValue;
    public Text dogTimerText;
    public Text girlTimerText;

    public float HordeTimeLeft;
    public float PreparationTimeLeft;
    
    private SpawnHandler sH;
    bool countDownPlaying;

	// Use this for initialization
	void Start () {
        scoreValue.text = LevelConfig.playerScore.ToString();
        PreparationTimer();
        HordeTimer();
        sH = GetComponent<SpawnHandler>();
        countDownPlaying = false;
    }

    private void PreparationTimer()
    {
        timerLabel.text = "Preparation end's in:";
        timerValue.text = LevelConfig.preparationTimer.ToString();
        PreparationTimeLeft = LevelConfig.preparationTimer;
    }

    private void HordeTimer()
    {
        timerValue.text = LevelConfig.hordeTimer.ToString();
        HordeTimeLeft = LevelConfig.hordeTimer;
    }

    // Update is called once per frame
    void Update () {
        if (LevelConfig.isPreparation)
        {
            PreparationTimerCountDown();
        }
        else
        {
            UpdatePlayerScore();
            if (!LevelConfig.isEndRound) {
                HordeTimerCountDown();
            }
        }
	}

    void PreparationTimerCountDown()
    {
        if (PreparationTimeLeft < 3f && !countDownPlaying)
        {
            audioSource.Play();
            StartCoroutine(playStarterSound());
            countDownPlaying = true;
        }

        PreparationTimeLeft -= Time.deltaTime;
        timerValue.text = Math.Round(PreparationTimeLeft).ToString();
        if (PreparationTimeLeft < 0)
        {
            //Debug.Log("Game Over!");
            if (LevelConfig.isPreparation)
            {
                // Notify dog and girl to start moving around
                Notify();
                timerLabel.text = "Horde Timer:";
                LevelConfig.isPreparation = false;
                LevelConfig.isEndRound = false;
            }
        }
    }

    IEnumerator playStarterSound()
    {
        yield return new WaitForSeconds(3f);
        audioSource.PlayOneShot(startSound);
    }

    private void Notify()
    {
        posDog.GetComponent<PositionHandler>().GeneratePosition();
        posGirl.GetComponent<PositionHandler>().GeneratePosition();
    }

    private void UpdatePlayerScore()
    {
        scoreValue.text = LevelConfig.playerScore.ToString();
    }

    private void HordeTimerCountDown()
    {
        HordeTimeLeft -= Time.deltaTime;
        timerValue.text = Math.Round(HordeTimeLeft).ToString();
        if (HordeTimeLeft < 0)
        {
            EndRoundScene();
        }
    }

    private void EndRoundScene()
    {
        audioSource.PlayOneShot(gongSound);
        LevelConfig.playerScore = int.Parse(scoreValue.text);
        endRoundPanel.SetActive(true);

        // Destroy hunters and Stop spawning
        LevelConfig.isEndRound = true;
        sH.destroyHunters();
        sH.CancelInvoke();
    }

    public void NextRound()
    {
        endRoundPanel.SetActive(false);
        PreparationTimer();
        HordeTimer();

        // Update LevelConfig;
        LevelConfig.isPreparation = true;
        LevelConfig.isEndRound = false;
        LevelConfig.round++;
        LevelConfig.screamRadius += LevelConfig.screamRadiusLevel * (0.1f);
        LevelConfig.playerMovementSpeed += LevelConfig.movementSpeedLevel * (0.3f);
        LevelConfig.hordeTimer++;


        if (LevelConfig.spawnFrequency > 0.2f)
        {
            LevelConfig.spawnFrequency -= LevelConfig.round * (0.2f);
        }

        countDownPlaying = false;
        sH.initHunters();
        sH.startSpawning();

        GameObject[] blocks = GameObject.FindGameObjectsWithTag("Block");
        GameObject[] traps = GameObject.FindGameObjectsWithTag("Trap");

        for (int i = 0; i < blocks.Length; i++)
        {
            Destroy(blocks[i]);
        }

        for (int i = 0; i < traps.Length; i++)
        {
            Destroy(traps[i]);
        }
    }
}
