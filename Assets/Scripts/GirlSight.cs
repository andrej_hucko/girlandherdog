﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GirlSight : MonoBehaviour {

    float timeLeft;
    float girlSightRadius = 0.5f;
    bool isScared = false;

    AudioSource audioSource;
    public GameObject girlTimerLabel;
    public GameObject girlTimerValue;

    // Use this for initialization
    void Start () {
        timeLeft = LevelConfig.girlScaredTime;
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!isScared)
        {
            CheckForHunters();
        }
        else
        {
            girlTimerLabel.SetActive(true);
            girlTimerValue.SetActive(true);
            girlTimerValue.GetComponent<Text>().text = Math.Round(timeLeft).ToString();
            StartDangerTime();
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            secureGirl();
        }
    }

    void StartDangerTime()
    { 
        timeLeft -= Time.deltaTime;
    }

    public float getTimeLeft()
    {
        return timeLeft;
    }

    public void secureGirl()
    {
        timeLeft = LevelConfig.girlScaredTime;
        girlTimerLabel.SetActive(false);
        girlTimerValue.SetActive(false);
        StartDangerTime();
        isScared = false;
    }

    private void CheckForHunters()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, girlSightRadius);
        var i = 0;

        while (hitColliders.Length > i)
        {
            if (hitColliders[i].gameObject.tag == "enemy")
            {
                audioSource.Play();
                StartDangerTime();
                isScared = true;
                break;
            }
            i++;
        }
    }
}
