﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicator : MonoBehaviour {

    public Transform target;
    public float HideDistance;
	
	// Update is called once per frame
	void Update () {
        var direction = target.position - transform.position;

        if (direction.magnitude < HideDistance)
        {
            SetArrowActive(false);
        }
        else
        {
            SetArrowActive(true);
        }
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}

    private void SetArrowActive(bool v)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(v);
        }
    }
}
