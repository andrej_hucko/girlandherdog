﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Rigidbody2D rb;
    public Animator animator;
    public Joystick joystick;

    bool left = false;
    float horizontal;
    float vertical;
    bool isPlaying = false;

    Vector2 touchOrigin = -Vector2.one;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {

    }
    // Update is called once per frame
    void FixedUpdate ()
    {
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        AnimatePlayer(horizontal, vertical);

#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE || UNITY_REMOTE
        horizontal = joystick.Horizontal;
        vertical = joystick.Vertical;

        AnimatePlayer(horizontal, vertical);
#endif
        rb.velocity = new Vector2(Mathf.Lerp(0, horizontal * LevelConfig.playerMovementSpeed, 0.8f),
                                                Mathf.Lerp(0, vertical * LevelConfig.playerMovementSpeed, 0.8f));
    }

    private void AnimatePlayer(float moveHorizontal, float moveVertical)
    {
        if (moveHorizontal != 0f)
        {
            animator.SetFloat("speed", Mathf.Abs(moveHorizontal));
            if (moveHorizontal < 0 && left == false)
            {
                transform.eulerAngles = transform.eulerAngles - 180f * Vector3.up;
                left = true;
            }
            else
            {
                if (moveHorizontal >= 0 && left == true)
                {
                    transform.eulerAngles = transform.eulerAngles + 180f * Vector3.up;
                    left = false;
                }
            }
        }
        else
        {
            if (moveVertical != 0f)
            {
                animator.SetFloat("speed", Mathf.Abs(moveVertical));
                if (moveHorizontal < 0f && left == false)
                {
                    left = true;
                    transform.eulerAngles = transform.eulerAngles - 180f * Vector3.up;
                }
                else
                {
                    if (moveHorizontal >= 0f && left == true)
                    {
                        transform.eulerAngles = transform.eulerAngles + 180f * Vector3.up;
                        left = false;
                    }
                }
            }
            else
            {
                animator.SetFloat("speed", Mathf.Abs(0f));
            }
        }
    }
}
