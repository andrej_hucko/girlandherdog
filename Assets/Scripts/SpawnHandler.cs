﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHandler : MonoBehaviour {

    [SerializeField]
    GameObject enemyPrefab;
    [SerializeField]
    GameObject spawnLocation;

    List<GameObject> hunters;
    float spawnRadius = 6f;
    Bounds spawnBounds;
    float minX;
    float maxX;
    float minY;
    float maxY;

	// Use this for initialization
	void Start () {
        hunters = new List<GameObject>();

        spawnBounds = spawnLocation.GetComponent<SpriteRenderer>().bounds;
        minX = spawnBounds.min.x;
        maxY = spawnBounds.max.y;
        maxX = spawnBounds.max.x;
        minY = spawnBounds.min.y;
        startSpawning();
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void GenerateHunters()
    {
        var numberOfEnemies = Random.Range(1, 1 + LevelConfig.round);
        if (!LevelConfig.isPreparation && !LevelConfig.isEndRound)
        {
            for (int i = 0; i < numberOfEnemies; i++)
            {
                Vector2 spawnLocation = Random.insideUnitCircle.normalized * spawnRadius; /*new Vector2(Random.Range(minX, maxX), maxY);*/
                GameObject newHunter = Instantiate(enemyPrefab, spawnLocation, Quaternion.identity);
                newHunter.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                newHunter.GetComponent<EnemyAI>().source = GameObject.Find("RunLocation").transform;

                hunters.Add(newHunter);
            }
        }
    }

    public List<GameObject> getHunters()
    {
        return hunters;
    }
    public void initHunters()
    {
        hunters = new List<GameObject>();
    }

    public void destroyHunters()
    {
        for (int i = 0; i < hunters.Count; i++)
        {
            Destroy(hunters[i]);
        }
        hunters = null;
    }

    public void startSpawning()
    {
        InvokeRepeating("GenerateHunters", 2f, LevelConfig.spawnFrequency);
    }
}
