﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct Highscore
{
    public string username;
    public int score;
    public string date;

    public Highscore(string _username, int _score, string _date)
    {
        username = _username;
        score = _score;
        date = _date;
    }

}

public class HighScores : MonoBehaviour {

    public GameObject content;
    public GameObject item;

    public InputField inputUsername;

    const string privateCode = "7auy8ZEpa0-2IvUViTIKWwxcI2BEqZrk-bvIOdwSe_fA";
    const string publicCode = "5c3b3f52b6397e0c2434d4ce";
    const string url = "http://dreamlo.com/lb/";

    public Highscore[] highscoresList;

    private void Start()
    {
         DownloadHighscores();
    }

    // On Submit button
    public void SubmitScore()
    {
        AddNewHighscore(inputUsername.text, LevelConfig.playerScore);
    }

    public void AddNewHighscore(string username, int score)
    {
        StartCoroutine(UploadNewHighscore(username, score));
    }

    IEnumerator UploadNewHighscore(string username, int score)
    {
        WWW www = new WWW(url + privateCode + "/add/" + WWW.EscapeURL(username) + "/" + score);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
            print("Upload Successful");
        else
        {
            print("Error uploading: " + www.error);
        }
    }

    public void DownloadHighscores()
    {
        StartCoroutine("DownloadHighscoresFromDatabase");

    }

    IEnumerator DownloadHighscoresFromDatabase()
    {
        WWW www = new WWW(url + publicCode + "/pipe/");
        yield return www;

        if (string.IsNullOrEmpty(www.error))
            FormatHighscores(www.text);
        else
        {
            print("Error Downloading: " + www.error);
        }
    }

    void FormatHighscores(string textStream)
    {
        string[] entries = textStream.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
        highscoresList = new Highscore[entries.Length];

        for (int i = 0; i < entries.Length; i++)
        {
            string[] entryInfo = entries[i].Split(new char[] { '|' });
            string username = entryInfo[0];
            int score = int.Parse(entryInfo[1]);
            string date = entryInfo[4];
            highscoresList[i] = new Highscore(username, score, date);
            Debug.Log(highscoresList[i].username + ": " + highscoresList[i].score + " Date: " + highscoresList[i].date);

            var copy = Instantiate(item);
            copy.transform.parent = content.transform;
            copy.GetComponent<Text>().text = (i + 1) + ". " + highscoresList[i].username + ": " + highscoresList[i].score + " points achieved on Date: " + highscoresList[i].date;
        }
    }
}

