﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusHandler : MonoBehaviour {

    public Text basicTrapCounter;
    public Text playerScore;
    public Text smallBricksCounter;

    void Update()
    {
        UpdateBonusCounts();
    }

    private void UpdateBonusCounts()
    {
        basicTrapCounter.text = LevelConfig.basicTrapsCount.ToString();
        smallBricksCounter.text = LevelConfig.smallBrickCount.ToString();
    }

    // Movement Speed
    public void IncreaseMsLevel()
    {
        var newBalance = LevelConfig.playerScore - LevelConfig.movementSpeedCost;
        if (newBalance >= 0)
        {
            LevelConfig.movementSpeedLevel++;
            LevelConfig.playerScore = newBalance;
            playerScore.text = LevelConfig.playerScore.ToString();
        }
    }
    // Scream Radius
    public void IncreaseSrLevel()
    {
        var newBalance = LevelConfig.playerScore - LevelConfig.screamRadiusCost;
        if (newBalance >= 0)
        {
            LevelConfig.screamRadiusLevel++;
            LevelConfig.playerScore = newBalance;
            playerScore.text = LevelConfig.playerScore.ToString();
        }
        
    }
    // Basic Traps
	public void IncreaseBasicTraps()
    {
        if (LevelConfig.playerScore - LevelConfig.basicTrapsCost >= 0)
        {
            LevelConfig.basicTrapsCount++;
            LevelConfig.playerScore -= LevelConfig.basicTrapsCost;
            basicTrapCounter.text = LevelConfig.basicTrapsCount.ToString();
            playerScore.text = LevelConfig.playerScore.ToString();
        }
    }

    // Small Bricks
    public void IncreaseSmallBricks()
    {
        if (LevelConfig.playerScore - LevelConfig.smallBrickCost >= 0)
        {
            LevelConfig.smallBrickCount++;
            LevelConfig.playerScore -= LevelConfig.smallBrickCost;
            smallBricksCounter.text = LevelConfig.smallBrickCount.ToString();
            playerScore.text = LevelConfig.playerScore.ToString();
        }
    }
}
