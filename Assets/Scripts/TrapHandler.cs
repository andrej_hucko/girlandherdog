﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapHandler : MonoBehaviour {

    public Animator animator;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("ScreamingAbillity", 2f, 2f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void ScreamingAbillity()
    {
        animator.SetTrigger("StartScream");

        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, LevelConfig.screamRadius);
        int i = 0;

        while (i < hitColliders.Length)
        {
            if (hitColliders[i].gameObject.tag == "enemy")
            {
                LevelConfig.playerScore += LevelConfig.acquirePoints;
                hitColliders[i].SendMessage("MakeScared");
            }
            i++;
        }
    }
}