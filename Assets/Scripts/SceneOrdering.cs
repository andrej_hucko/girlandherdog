﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneOrdering : MonoBehaviour {

	// Use this for initialization
	void Update() {
        GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(transform.position.y * 100f) * -1;
    }
}
