﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStarter : MonoBehaviour {

	public void StartLevel()
    {
        LoadData();
        SceneManager.LoadScene(1);
    }

    void LoadData()
    {
        // Loading JSON file of data informations about lvls
    }
}
