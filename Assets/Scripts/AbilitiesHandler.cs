﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilitiesHandler : MonoBehaviour {

    [SerializeField]
    Animator animator;
    [SerializeField]
    int raysToShoot = 100;
    [SerializeField]
    GameObject cam;

    public GameObject trapPrefab;
    public GameObject smallBrickPrefab;

    AudioSource audioSource;


    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            UnleashScream();
        }
	}

    public void UnleashScream()
    {
        animator.SetTrigger("StartScream");
        cam.GetComponent<CameraShake>().shakeDuration = 0.5f;

        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, LevelConfig.screamRadius);
        int i = 0;

        while (i < hitColliders.Length)
        {
            if (hitColliders[i].gameObject.tag == "enemy")
            {
                LevelConfig.playerScore += LevelConfig.acquirePoints;
                hitColliders[i].SendMessage("MakeScared");
            }
            i++;
        }
        audioSource.Play();
    }

    public void SpawnTrap()
    {
        if (LevelConfig.basicTrapsCount > 0)
        {
            Vector2 spawnLocation = transform.position;
            GameObject trap = Instantiate(trapPrefab, spawnLocation, Quaternion.identity);
            LevelConfig.basicTrapsCount--;
        }
    }

    public void SpawnSmallBrick()
    {
        if (LevelConfig.smallBrickCount > 0)
        {
            Vector2 spawnLocation = new Vector2(transform.position.x + 0.5f, transform.position.y);
            GameObject trap = Instantiate(smallBrickPrefab, spawnLocation, Quaternion.identity);
            LevelConfig.smallBrickCount--;
        }
    }
}
