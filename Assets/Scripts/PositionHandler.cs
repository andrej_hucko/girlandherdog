﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionHandler : MonoBehaviour
{

    [SerializeField] GameObject dog;
    [SerializeField] GameObject background;
    // Sets the position to be somewhere inside a circle
    [SerializeField] float movementRadius = 5.0f;
    [SerializeField] float timeDelay = 1.0f;

    DogController dogController;
    Bounds backgroundBounds;
    Vector2 positionToGo;
    //Vector2 preparingPosition;

    // Use this for initialization
    void Start()
    {
        dogController = dog.GetComponent<DogController>();
        // Scale bounds of sprite to match the size of sprite
        backgroundBounds = background.GetComponent<SpriteRenderer>().sprite.bounds;
        backgroundBounds.extents = backgroundBounds.extents * background.transform.localScale.x;
        Debug.Log("Max: " + backgroundBounds.max + " Min: " + backgroundBounds.min);

        //GeneratePosition();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        GeneratePosition();
    }

    public void GeneratePosition()
    {
        // Generate random position in movementRadius
        positionToGo = UnityEngine.Random.insideUnitSphere * movementRadius + dog.transform.position;
        //Debug.Log(positionToGo + " " + backgroundBounds.Contains(positionToGo));
        // Check if it is in the bounds of background
        while (!backgroundBounds.Contains(positionToGo))
        {
            // Debug.Log("Bounds exceeded generate new position inside!");
            positionToGo = UnityEngine.Random.insideUnitSphere * movementRadius + dog.transform.position;
        }
        // Set position of destination  
        transform.position = positionToGo;

        // Call dog to move to destination
        StartCoroutine(dogController.SetTargetPosition(positionToGo, timeDelay));
    }
}
