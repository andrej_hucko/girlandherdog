﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameHandler : MonoBehaviour {
    [SerializeField]
    GameObject dog;
    DogController dogController;
    SpawnHandler spawnHandler;
    [SerializeField]
    GirlSight girlSight;

    int activeHunters;
    float catchingDistance = 0.2f;

    // Use this for initialization
    void Start () {
        activeHunters = 0;
        spawnHandler = GetComponent<SpawnHandler>();
        dogController = dog.GetComponent<DogController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        CheckDog();
        CheckDangerTimeLeft();
    }

    private void CheckDangerTimeLeft()
    {
        if (dogController.getTimeLeft() < 1f)
        {
            LevelConfig.dogCaught = true;
            // Transit to EndGameScene
            SceneManager.LoadScene(2);
        }

        if (girlSight.getTimeLeft() < 1f)
        {
            LevelConfig.girlScared = true ;
            SceneManager.LoadScene(2);
        }
    }

    private void CheckDog()
    {
        var hunters = spawnHandler.getHunters();
        if (hunters != null)
        {
            foreach (GameObject hunter in spawnHandler.getHunters())
            {
                var hunterDistanceFromDog = Vector2.Distance(hunter.transform.position, dog.transform.position);
                if (hunterDistanceFromDog < catchingDistance)
                {
                    activeHunters++;
                }
            }
        }

        if (activeHunters != 0)
        {
            dogController.setState(DogController.DogState.InDanger);
        }
        else
        {
            dogController.setState(DogController.DogState.Safe);
        }
        activeHunters = 0;
    }
}
