﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DogController : MonoBehaviour {

    // From LevelConfig static class
    [NonSerialized]
    public float dogLiveTime;
    public float timeLeft;

    AudioSource audioSource;
    bool isPlaying = false;
    // Speed in units per sec.
    [SerializeField] float movementSpeed = 5.0f;
    float step;

    [NonSerialized]
    public Vector2 targetPosition;
    public GameObject dogTimerLabel;
    public GameObject dogTimerValue;

    // States of a dog
    public enum DogState
    {
        Safe, InDanger
    };
    Collider2D hunterAttacking;
    DogState actualState;

    void Start()
    {
        actualState = DogState.Safe;
        dogLiveTime = LevelConfig.dogLiveTime;
        timeLeft = dogLiveTime;
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        Debug.Log(actualState);
        if (actualState == DogState.InDanger && !LevelConfig.isEndRound)
        {
            dogTimerLabel.SetActive(true);
            dogTimerValue.SetActive(true);
            if (!isPlaying)
            {
                audioSource.Play();
                isPlaying = true;
            }
            dogTimerValue.GetComponent<Text>().text = Math.Round(timeLeft).ToString();
            StartDangerTime();
        }
        else
        {
            isPlaying = false;
            dogTimerLabel.SetActive(false);
            dogTimerValue.SetActive(false);
            timeLeft = dogLiveTime;
        }
    }

    void FixedUpdate()
    {
        // The step size is equal to speed times frame time.
        float step = movementSpeed * Time.deltaTime;
        if (actualState == DogState.Safe)
        {
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, step);
        }
    }

    void StartDangerTime()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            //Debug.Log("Game Over!");
        }
    }

    public IEnumerator SetTargetPosition(Vector2 positionToGo, float delayTime)
    {
        // Debug.Log(positionToGo);
        yield return new WaitForSeconds(delayTime);
        targetPosition = positionToGo;
    }

    public void setState(DogState state)
    {
        actualState = state;
    }

    public DogState getState()
    {
        return actualState;
    }
    public float getTimeLeft()
    {
        return timeLeft;
    }
}
