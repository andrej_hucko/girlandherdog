﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TryAgain : MonoBehaviour {

    public Text gameOverText;
    public Text informationText;

    public AudioSource audioSource;
    public AudioClip end1;
    public AudioClip end2;

	// Use this for initialization
	void Start () {
        if (LevelConfig.dogCaught)
        {
            gameOverText.text = "Game Over - Dog has been caught!";
        }
        else
        {
            gameOverText.text = "Game Over - Girl recognized the hunters!";
        }
        informationText.text = "You lasted " + LevelConfig.round + " rounds with a score of " + LevelConfig.playerScore + " points";

        audioSource.PlayOneShot(end1);
        audioSource.PlayOneShot(end2);
	}
	
	public void Restart()
    {
        LevelConfig.ResetData();
        SceneManager.LoadScene(0);
    }
}
