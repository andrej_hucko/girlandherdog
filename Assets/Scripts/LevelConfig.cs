﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelConfig {
    // Basic information
    public static int round = 1;
    public static int playerScore = 0;

    //Abilities info
    public static float movementSpeedLevel = 1f;
    public static float playerMovementSpeed = 1f;
    public static int movementSpeedCost = 2000;

    public static int screamRadiusLevel = 1;
    public static float screamRadius = 0.2f;
    public static int screamRadiusCost = 3000;

    // Gadget info
    public static int basicTrapsCount = 0;
    public static int basicTrapsCost = 1000;
    public static int smallBrickCount = 0;
    public static int smallBrickCost = 100;

    // Level Timers
    public static float dogLiveTime = 10f;
    public static float girlScaredTime = 12f;
    public static float preparationTimer = 10f;
    public static float hordeTimer = 45f;

    // Enemies information
    public static float spawnFrequency = 4f;
    public static int acquirePoints = 20;

    // Helpers
    public static bool isPreparation = true;
    public static bool isEndRound = true;

    public static bool dogCaught = false;
    public static bool girlScared = false;

    public static void ResetData()
    {
        round = 1;
        playerScore = 0;
        movementSpeedLevel = 1;
        playerMovementSpeed = 1f;
        movementSpeedCost = 2000;
        screamRadiusLevel = 1;
        screamRadius = 0.2f;
        screamRadiusCost = 3000;
        basicTrapsCount = 0;
        basicTrapsCost = 1000;
        smallBrickCount = 0;
        smallBrickCost = 100;
        dogLiveTime = 10f;
        girlScaredTime = 10f;
        preparationTimer = 10f;
        hordeTimer = 45f;
        spawnFrequency = 4f;
        acquirePoints = 20;
        isPreparation = true;
        isEndRound = true;
        dogCaught = false;
        girlScared = false;
    }
}
